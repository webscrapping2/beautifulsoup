from bs4 import BeautifulSoup
import requests

def scrap_links(url):
    response = requests.get(url)
    soup = BeautifulSoup(response.content,'lxml')
    page_links = []
    with open('nature.txt','w') as file :
        num_list = []
        base_url ='https://www.nature.com/bmt/articles?searchType=journalSearch&sort=PubDate&year=2023&page='

        for page_element in soup.find_all('a',{'class':'c-pagination__link'}):
            href = page_element['href']
            print(href)
            split = href.split("page=")
            num_list.append(split[1])
        maxs1 = min(num_list)
        print(maxs1)
    
        page_range = range(1,int(maxs1)+1)

        for page in page_range  :
            urls = base_url + str(page)
            print("urls", urls)
            file.write((urls+"\n"))

    with open('natures.txt','w')as f :
        for article_pages in page_links:
            article_response = requests.get(article_pages)
            article_soup = BeautifulSoup(article_response.content, 'lxml')
            for article in article_soup.find_all('a',class_="c-card__link u-link-inherit") :
                article_link = article['href']
                #print(article_link)
                f.write(("https://www.nature.com"+article_link)+"\n")
        
scrap_links('https://www.nature.com/bmt/articles?year=2023')

