import requests
from bs4 import BeautifulSoup


def scrapping_links():
   url="https://www.jstage.jst.go.jp/browse/mej/list/-char/en"
   response = requests.get(url=url)
   soup = BeautifulSoup(response.text,'lxml')
         
   volume_links =[]
   for vlink in soup.find_all('a',{"class":"bluelink-style vol-link"}):
      volume_links.append(vlink.get('href'))
      
      
   pattern = 'https://www.jstage.jst.go.jp/article/mej'     
   issue_link = []
   with open('ragul.txt','x') as file :
      for ilinks in soup.find_all('a',{"class":'bluelink-style customTooltip'}):
         link= ilinks.get('href')
         issue_link.append(ilinks['href'])
         
         if link and link.startswith(pattern) :
            file.write(link +'\n')
                  
   file= open ("ragul.txt","a") 
   for issue in issue_link :
      article_response = requests.get(issue)
      article_soup = BeautifulSoup(article_response.text,'lxml')
      for alink in article_soup.find_all('a',{"class":"bluelink-style customTooltip"}):
         article = alink["href"]
         if article.startswith(pattern):
            file.write(article +'\n')
            
if __name__ =='__main__' :
 scrapping_links()               
        