from bs4 import BeautifulSoup
import requests

def scrape_links(url):
    response = requests.get(url)
    soup = BeautifulSoup(response.content,'lxml')
    
    page_links=[]
    with open('nature_links.txt','w') as file :
        base_url ='https://www.nature.com/bmt/articles?searchType=journalSearch&sort=PubDate&year=2023&page='
        num_list=[]
        for page_range in soup.find_all('a',{'class':'c-pagination__link'}):
            href = page_range['href']
           # print(href)
            split = href.split("page=")
            num_list.append(split[1])
        last_element = num_list[-2]
        print(last_element)
    
        page_range = range(1,int(last_element)+1)
        #     page_ranges = page_range.get_text(strip=True)
        #     num.append(page_ranges)
        #     print(num)
        # pageno = num[2]
        # page1 = pageno[4:]
        # print(page1) 
        #page_range = range(1,int(page1)+1)
        for page in page_range  :
           # print(page)
            urls = base_url + str(page)
            print(urls)
            page_links.append(urls)
            file.write(urls + '\n')
    

    with open('nature_article_links.txt','w')as f :
        for article_pages in page_links:
            article_response = requests.get(article_pages)
            article_soup = BeautifulSoup(article_response.content, 'lxml')
            for article in article_soup.find_all('a',class_="c-card__link u-link-inherit") :
                article_link = article['href']
                #print(article_link)
                f.write(("https://www.nature.com"+article_link)+"\n")
        
scrape_links('https://www.nature.com/bmt/articles?year=2023')

    
    
    
    
    